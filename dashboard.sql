-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 18 2021 г., 19:31
-- Версия сервера: 10.3.25-MariaDB-0ubuntu0.20.04.1-log
-- Версия PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `dashboard`
--

-- --------------------------------------------------------

--
-- Структура таблицы `dashboard`
--

CREATE TABLE `dashboard` (
  `id` int(11) NOT NULL,
  `lpu_name` varchar(255) NOT NULL,
  `director_name` varchar(255) NOT NULL,
  `director_photo` varchar(100) NOT NULL,
  `param1` decimal(10,1) NOT NULL DEFAULT 0.0,
  `param2` decimal(10,1) NOT NULL DEFAULT 0.0,
  `param3` decimal(10,1) NOT NULL DEFAULT 0.0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `dashboard`
--

INSERT INTO `dashboard` (`id`, `lpu_name`, `director_name`, `director_photo`, `param1`, `param2`, `param3`) VALUES
(1, 'ГБУЗ АО АМОКБ', 'Калашников Евгений Сергеевич', '1.jpg', '100.0', '95.0', '18.0'),
(2, 'ГБУЗ АО ГКБ №2', 'Якушев Руслан Борисович', '1.jpg', '56.0', '75.0', '95.0'),
(3, 'ГБУЗ АО ГКБ №3', 'Юлушев Байрам-Ажи Гусейнович', '1.jpg', '65.0', '50.0', '100.0'),
(4, 'ГБУЗ АО ОДКБ', 'Яснопольский Юрий Валерьевич', '1.jpg', '89.0', '98.0', '43.5'),
(5, 'ГБУЗ АО ОИКБ', 'Ларина Надежда Николаевна', '1.jpg', '65.2', '20.0', '1.0'),
(6, 'ГБУЗ АО ОКПБ', 'Тарханов Владимир Саввич', '1.jpg', '100.0', '98.0', '99.0');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `dashboard`
--
ALTER TABLE `dashboard`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `dashboard`
--
ALTER TABLE `dashboard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
