<?php

$parameters = [
    'param1' => 'Показатель 1',
    'param2' => 'Показатель 2',
    'param3' => 'Показатель 3',
];

$default_parameter = array_keys($parameters)[0];
$get_param = (array_key_exists('parameter', $_GET))? $_GET['parameter'] : $default_parameter;
$current_parameter = (array_key_exists($get_param, $parameters))? $get_param : $default_parameter;

$mysql = new PDO('mysql:dbname=dashboard;host=localhost',
    'dashboard',
    'dashboard',
    array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'")

);

$sql = "SELECT id, lpu_name, director_name, director_photo, ".$current_parameter." as 'percent' FROM dashboard ORDER BY percent DESC, lpu_name";
$sth = $mysql->prepare($sql);
$sth->execute();
$lpu_array = $sth->fetchAll(PDO::FETCH_ASSOC);
