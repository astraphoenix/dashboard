<?php require_once "dashboard_processor.php"; ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <style>
        .success-card-background {
            background-color: MediumSpringGreen;
        }
        .bad-card-background {
            background-color: LightCoral;
        }
    </style>

    <title><?=$parameters[$current_parameter]?></title>

</head>
<body>
<div class="container">

    <div class="row">
        <?php foreach ($parameters as $key=>$value): ?>
        <a href="?parameter=<?=$key?>" class="btn btn-<?=($key==$current_parameter)?'primary':'secondary'?> m-1" ><?=$value?></a>
        <?php endforeach; ?>
    </div>
    <div class="row">
        <h1><?=$parameters[$current_parameter]?></h1>
    </div>
    <div class="row">
        <?php foreach ($lpu_array as $lpu): ?>
        <div class="card <?=($lpu['percent']>80)?'success-card-background':'bad-card-background'?>" style="width: 18rem;margin:10px" >
            <img src="director_photos/<?=$lpu['director_photo']?>" class="card-img-top" alt="<?=$lpu['director_name']?>">
            <div class="card-body">
                <h5 class="card-title  text-center"><?=$lpu['lpu_name']?></h5>
                <p class="card-text  text-center"><?=$lpu['lpu_name']?></p>
                <h1 class="card-text text-center"><?=(float)$lpu['percent'] ?>%</h1>
            </div>
        </div>
        <?php endforeach; ?>
    </div>
</div>


<script src="js/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>


</body>
</html>
